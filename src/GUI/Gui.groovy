/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI

import javax.swing.*
import javax.swing.JCheckBox
import javax.swing.JOptionPane
import javax.swing.table.DefaultTableModel
import java.awt.*
import java.awt.FlowLayout as FL
import java.awt.GridLayout as GL
import java.awt.GridBagLayout as GBL
import java.awt.event.ActionEvent
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.LocalTime
import java.awt.BorderLayout as BL
import javax.swing.table.*
import groovy.swing.*
import javax.swing.ScrollPaneConstants.*
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Abdelsalam Shahlol
 */
class Gui {
    SwingBuilder sb = new SwingBuilder()
    def hours = (1..12).collect{if(it<10){String.format("%02d", it)}else{it}}  
    def sec=(0..59).collect{if(it<10){String.format("%02d", it)}else{it}}   
    def min = (0..59).collect{if(it<10){String.format("%02d", it)}else{it}}   
    def sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a")
    def df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    def c=50000
    final Runnable focusSound = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation")
    def proBar
    
    def frame=sb.frame(title:"Shutdown Timer For Windows",
        size: [500,180],
        resizable: false,
        show:false,
        locationRelativeTo: null,
        //        windowClosed: {inventoryInstance=false},
        defaultCloseOperation:WindowConstants.DISPOSE_ON_CLOSE){
        panel(layout: new BL(), constraints: BL.NORTH){
            panel(layout: new FL()){
                panel(layout: new GL(3,1,0,0)){
                    label(text:"Set the machine shutdown time:",horizontalAlignment:JLabel.CENTER)
                    panel(layout: new GL(1,4,15,10)){
                        panel(layout: new GL(1,2)){
                            label(text:"Hours",horizontalAlignment:JLabel.CENTER)
                            comboBox(id: 'hrs', items: hours)
                        }
                        panel(layout: new GL(1,2)){
                            label(text:"Minutes",horizontalAlignment:JLabel.CENTER)
                            comboBox(id: 'mins',items:min)
                        }
                        panel(layout: new GL(1,2)){
                            label(text:"Seconds",horizontalAlignment:JLabel.CENTER)
                            comboBox(id: 'secs',items:sec)  
                        
                        }
                        panel(layout: new GL(1,2)){
                            comboBox(id: 'ante',items:["PM","AM"])   
                        }
                    }
                }
              
            }
        }
        panel(layout: new BL(),constraints:BL.CENTER){
 
            panel(layout: new FL()){
                label(text:"Actions")
                button(id:"start",text:"Set", actionPerformed:{
                        abrt.setEnabled(true)         
                        def timeString=hrs.getSelectedItem()+":"+mins.getSelectedItem()+":"+secs.getSelectedItem()+" "+ante.getSelectedItem()
                        println timeString
                        //                        LocalTime t = LocalTime.parse(timeString)
                        Date timeStingObj = sdf.parse("2018-5-6 "+timeString+" PM");
                        def userTimeObj = sdf.format(timeStingObj)
                        println userTimeObj
                        //                        Date timeStingObjEp = sdf.parse(timeString);

                        long epoch = timeStingObj.getTime();
                        //                        println "ep ${(int)(epoch)}"
                        def max = epoch
                        println "MAX  ${max}"
                        Runnable update = new Runnable() {
                            @Override
                            public void run() {
                                proBar.setMaximum((int)max.div(1000))
                            }
                        };
                        if (SwingUtilities.isEventDispatchThread()) {
                            update.run();
                        } else {
                            SwingUtilities.invokeLater(update);
                        }
                        println "secs ${timeStingObj.getTime().div(1000)}"
                       
                        def timer=new Timer().scheduleAtFixedRate(new TimerTask(){
                                @Override
                                public void run(){
                                    def date = new Date()
                                    long epochl = date.getTime();
                                    def timeStamp = sdf.format(date)
                                    println "ep ${epoch.div(1000)}"
                                    println "e2 ${(int)(epochl/1000)}"
                                    //                                    doOutside{
                                    //                                    try{
                                    //c=//50000//epochl
                                    //(int)(epochl/1000)
                                    Runnable u = new Runnable() {
                                        @Override
                                        public void run() {
                                            proBar.setValue((int)c)
                                            proBar.repaint()
                                            c+=10000000                           
                                        }
                                    };
                                    if (SwingUtilities.isEventDispatchThread()) {
                                        u.run();
                                    } else {
                                        SwingUtilities.invokeLater(u);
                                    }
                                    println "${max} CCCC ${c}"
                                    if(timeStamp==userTimeObj){
                                        if (focusSound != null)focusSound.run()
                                        def r= "cmd /c shutdown /s /t 0".execute()
                                        println r.text
                                        sb.optionPane().showMessageDialog(null,"Shutting Down... Goodbye!!.","Information",JOptionPane.INFORMATION_MESSAGE)
                                    }
                                    println( "current time ${timeStamp}");
                                }
                            },0,1000);
                    })
                button(id:"abrt",text:"Abort",enabled:false ,actionPerformed:{
                        proBar.setValue(0)
                        def r= "cmd /c shutdown /a".execute()
                        println r.text
                        sb.optionPane().showMessageDialog(null,"Shutdown Aborted :D","Information",JOptionPane.INFORMATION_MESSAGE)
                        abrt.setEnabled(false)         
                    }
                )
            }
        }
        panel(layout: new BL(),constraints:BL.SOUTH){
            proBar = progressBar(id:"proBar",stringPainted:true)
        }
    }
}

