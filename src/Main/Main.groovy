/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Main

import GUI.Gui
import de.javasoft.plaf.synthetica.SyntheticaLookAndFeel
import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel
import javax.swing.LookAndFeel
import javax.swing.UIManager
import java.awt.*
 
/**
 *
 * @author Abdelsalam Shahlol
 */
class Main {
    public static void main(String[] args){
        final String[] li = ["Licensee=RapidShare AG", "LicenseRegistrationNumber=102001", "Product=Synthetica", "LicenseType=Enterprise Site License", "ExpireDate=--.--.----", "MaxVersion=2.999.999"]
        UIManager.put("Synthetica.license.info", li)
        UIManager.put("Synthetica.license.key", "075DD94D-50F2C1E8-B7628175-3031CB2D-F8E8033E")
        UIManager.put("Synthetica.window.decoration", true)
        UIManager.put("synthetica.painter.RootPanePainter;", true)
        UIManager.put("Synthetica.focus.textComponents.enabled", true)
        UIManager.put("Synthetica.extendedFileChooser.rememberPreferences", Boolean.TRUE)
        UIManager.put("Synthetica.tree.line.type", "NONE")
        SyntheticaLookAndFeel.setLookAndFeel(SyntheticaPlainLookAndFeel.class.getName())
       
        EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() throws Exception{ 
                    try{
                        def g = new Gui()
                        g.frame.setVisible(true)
                        
                    }catch(Exception e){
                        def now = new Date()
                        def time = now.format("yyyyMMdd_HH_mm_ss_SSS", TimeZone.getTimeZone('UTC'))
                        def  file = new File(time+".txt")
                        file<<"Message\r\n"+e.getMessage()
                        def stack=e.printStackTrace()
                        if(stack!=null && stack!=""){
                            file<<"Stack Trace\r\n"+stack
                        }
                    }
                }
            })
    }
}

